from pygraph.graph import*

windowSize(1200, 800)
canvasSize(1200, 800)


def roof(x, y, w, h):
    """x, y координати вершини даха"""
    brushColor("firebrick")
    polygon([(x, y), (x+w/2, y+h), (x-w/2, y+h)])


def wall(x, y, w, h):
    penSize(5)
    brushColor("tomato")
    rectangle(x, y, x+w, y+h)


def chimney(x, y, w, h):
    rectangle(x, y, x+w, y+h)


def door(x, y, w, h):
    brushColor("firebrick")
    rectangle(x, y, x+w, y+h)


def window(x, y, w, h):
    brushColor("lightsalmon")
    rectangle(x, y, x+w, y+h)
    # horizontal
    line(x, y+h/2, x+w, y+h/2)
    # vertical
    line(x+w/2, y, x+w/2, y+h)


def hwindow(x, y, r):
    circle(x, y, r)
    line(x-r, y, x+r, y)
    line(x, y-r, x, y+r)


def bush(x, y, r):
    brushColor("palegreen")
    circle(x+r, y+r/3, r-r/3)
    circle(x-r, y+r/3, r-r/3)
    circle(x, y, r)


def house(x, y, h, w):
    wall_h = h/3*2
    wall_w = w-w/10
    wall_x = x+w/20
    wall_y = y+h/3
    wall(x=wall_x, y=wall_y, w=wall_w, h=wall_h)

    roof_x = x+w/2
    roof_y = y
    roof_w = w
    roof_h = h/3

    chimney_w = wall_w/10
    chimney_h = wall_h/3
    chimney_x = wall_x + roof_w/3
    chimney_y = roof_y - chimney_h/2
    chimney(chimney_x, chimney_y, chimney_w, chimney_h)

    roof(x=roof_x, y=roof_y, w=roof_w, h=roof_h)

    door_w = wall_w/6
    door_h = wall_h/2
    door_x = wall_x + wall_w/2 - door_w/2
    door_y = wall_h + wall_y - door_h
    door(door_x, door_y, door_w, door_h)

    window_w = wall_h/2.2
    window_h = window_w
    lwindow_x = wall_x + wall_w/10
    window_y = wall_y + wall_h/6
    rwindow_x = wall_x + wall_w - wall_w/10 - window_w
    window(lwindow_x, window_y, window_w, window_h)
    window(rwindow_x, window_y, window_w, window_h)
    hwindow(wall_x+wall_w/2, roof_y+roof_h/2, roof_h/5)

    bush(wall_x-wall_w/5, wall_y+wall_h/1.25, wall_h/4)
    bush(wall_x+wall_w+wall_w/5, wall_y+wall_h/1.25, wall_h/4)


if __name__ == "__main__":
    canvas_w = 1200
    canvas_h = 800
    windowSize(canvas_w, canvas_h)
    canvasSize(canvas_w, canvas_h)

    house(x=canvas_w/4, y=canvas_h/4, w=350, h=200)
    run()
